# Netzme Test User Api

Netzme - Backend Developer Technical Test
Create user API fetching data from API `https://randomuser.me/api/`

## Project Explanation
Project was created with Java Spring Boot Framework.
Request lists:
- Request GET: http://localhost:8080/api/user

Test unit was created for tested the on the service and controller level.
Test files:
- UserControllerTest.java
- UserServiceTest.java


created by Nuzulul Aulia Perdana Putra


