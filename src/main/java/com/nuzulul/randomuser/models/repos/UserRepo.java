package com.nuzulul.randomuser.models.repos;

import com.google.gson.JsonElement;

import retrofit2.Call;
import retrofit2.http.GET;

public interface UserRepo {

    @GET("/api")
    Call<JsonElement> getUser();

}
