package com.nuzulul.randomuser;

import com.nuzulul.randomuser.controllers.UserController;
import com.nuzulul.randomuser.models.entities.User;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.mockito.BDDMockito.given;
import static org.hamcrest.CoreMatchers.is;

// @SpringBootTest
@WebMvcTest(controllers = UserController.class)
public class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserController userController;

    private User user;

    @BeforeEach
    void setUp() throws Exception {
        this.user = new User("female", "Mrs Victoria Lemaire", "5664 Rue de L'Abbé-Grégoire Rouen", "https://randomuser.me/api/portraits/women/13.jpg");
    }

    /**
     * Test will check if the controller will return the expected User object
     * @throws Exception
     */
    @Test
    void shouldFetchUser_POSITIVE() throws Exception {
        given(userController.fetchUser()).willReturn(user);

        this.mockMvc.perform(MockMvcRequestBuilders.get("/api/user"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.gender", is(user.getGender())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.fullname", is(user.getFullname())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.address", is(user.getAddress())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.picture", is(user.getPicture())));
    }

    /**
     * Test will failed because no user will be shown in response
     * @throws Exception
     */
    @Test
    void shouldFetchUser_NEGATIVE() throws Exception {
        given(userController.fetchUser()).willReturn(null);

        this.mockMvc.perform(MockMvcRequestBuilders.get("/api/user"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.gender", is(user.getGender())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.fullname", is(user.getFullname())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.address", is(user.getAddress())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.picture", is(user.getPicture())));
    }
}
