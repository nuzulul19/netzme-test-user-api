package com.nuzulul.randomuser;

import com.nuzulul.randomuser.services.UserService;

import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.skyscreamer.jsonassert.comparator.ArraySizeComparator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class UserServiceTest {

	@Autowired
	private UserService userService;

	/**
	 * Test if userService fetchUser return jsonObject that contains results key with array of data
	 * @throws Exception
	 */
	@Test
	public void shouldJsonElements() throws Exception {
		// given
		String underTest = userService.fetchUser().toString();

		// when
		String expected = "{results: [1]}";

		// then
		JSONAssert.assertEquals(expected, underTest, new ArraySizeComparator(JSONCompareMode.LENIENT));
	}


}
