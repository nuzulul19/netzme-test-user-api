package com.nuzulul.randomuser.services;

import java.io.IOException;

import com.google.gson.JsonElement;
import com.nuzulul.randomuser.models.repos.UserRepo;

import org.springframework.stereotype.Service;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;


@Service
public class UserService {

    /**
     * Services to fetch data from API
     * @return json element in response
     * @throws IOException
     */
    public JsonElement fetchUser() throws IOException {
        Retrofit retrofit = RetrofitClientService.getRetrofitInstance();
        UserRepo userRepo = retrofit.create(UserRepo.class);
        Call<JsonElement> cUser = userRepo.getUser();
        Response<JsonElement> execute = cUser.execute();
        JsonElement jElement = execute.body();
        return jElement;
    }

}
