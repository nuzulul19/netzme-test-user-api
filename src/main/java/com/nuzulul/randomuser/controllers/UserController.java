package com.nuzulul.randomuser.controllers;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.nuzulul.randomuser.models.entities.User;
import com.nuzulul.randomuser.services.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * Get the user detail list from API
     * @return ResponseData
     * @throws Exception
     */
    @GetMapping
    public User fetchUser() {
        try {
            // Get JSON data from service
            JsonElement jElement = userService.fetchUser();
            JsonArray resultList = jElement.getAsJsonObject().getAsJsonArray("results");

            if (resultList.size() > 0) {
                // Get only first element of resultList
                return parseResultObj((JsonObject) resultList.get(0));
            }
            return null;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static User parseResultObj(JsonObject result) {
        JsonObject nameObject = (JsonObject) result.get("name");
        JsonObject pictureObject = (JsonObject) result.get("picture");
        JsonObject locationObject = (JsonObject) result.get("location");
        JsonObject streetObject = (JsonObject) locationObject.get("street");

        String gender = result.get("gender").getAsString();
        String picture = pictureObject.get("large").getAsString();
        String fullname = nameObject.get("title").getAsString()
                        + " " + nameObject.get("first").getAsString()
                        + " " + nameObject.get("last").getAsString();
        String address = streetObject.get("number").getAsString()
                        + " " + streetObject.get("name").getAsString()
                        + " " + locationObject.get("city").getAsString();

        return new User(gender, fullname, address, picture);
    }
}
